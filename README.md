# Auth0 for Plesk

This is a new project that will allow you to use SAML/Oauth2, provided by [Auth0](https://auth0.com/), to authenticate users when logging into the Plesk Web Hosting platform. We are still working on core functionality, and the full source code will be ready to publish in this repository by Friday of this week. More information will be added to this readme once the source code is published.

This project is developed using PHP and HTML, and also incorporates several elements from the Laravel framework.